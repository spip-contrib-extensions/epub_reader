<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-epubreader?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'epubreader_description' => 'Leer los archivos [ePUB->http://fr.wikipedia.org/wiki/EPUB_%28format%29] directamente en el sitio',
	'epubreader_nom' => 'Lector ePUB',
	'epubreader_slogan' => 'Leer los archivos ePUB'
);
