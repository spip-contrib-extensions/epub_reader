<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-epubreader?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'epubreader_description' => 'Lees [ePUB->http://nl.wikipedia.org/wiki/EPUB] bestanden direct in de site',
	'epubreader_nom' => 'ePUB Lezer',
	'epubreader_slogan' => 'ePUB bestanden lezen'
);
